import numpy as np
from numba import jit
import cv2
from PIL import Image

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 800
ZOOM_COUNT = 5
xmin = -1.5
xmax = 0.5
ymin = -1
ymax = 1
maxiter = 256
r1 = np.linspace(xmin, xmax, SCREEN_WIDTH)
r2 = np.linspace(ymin, ymax, SCREEN_HEIGHT)
n3 = np.empty((SCREEN_WIDTH, SCREEN_HEIGHT))

img = Image.new("RGB", (SCREEN_WIDTH, SCREEN_HEIGHT))
img = np.array(img)


@jit
def mandelbrot(c):
    z = c
    for n in range(maxiter):
        if abs(z) > 2:
            return n
        z = z * z + c
    return 0


@jit
def m_set():
    for x in range(SCREEN_WIDTH - 1):
        for y in range(SCREEN_HEIGHT - 1):
            img[x, y] = mandelbrot(r1[y] + 1j * r2[x])


m_set()
for _ in range(ZOOM_COUNT):
    print("Select region with mouse and press space.")
    roi = cv2.selectROI("Mandelbrot", img, False)
    # roi tuple formatted (top right corner pixel x, top right corner pixel y, distace x, distance y)
    # Find value at r1 and r2 with index that matches roi coordinates.
    r1 = np.linspace(r1[roi[0]], r1[roi[0] + roi[2]], SCREEN_WIDTH)
    r2 = np.linspace(r2[roi[1]], r2[roi[1] + roi[3]], SCREEN_HEIGHT)
    m_set()
print("Space again to exit")
cv2.imshow("Mendelbrot", img)
k = cv2.waitKey(0)

cv2.destroyAllWindows()
